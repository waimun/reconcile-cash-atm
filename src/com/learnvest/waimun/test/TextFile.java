package com.learnvest.waimun.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

public class TextFile implements CSVFileConstants {
	
	private static final Logger LOGGER = Logger.getLogger(TextFile.class.getName());
	
	public TextFile(String contents)
			throws IllegalArgumentException, SecurityException,
			RuntimeException, IOException {
		
		if (contents == null || contents.isEmpty()) {
			throw new IllegalArgumentException("Contents to write is null or empty.");
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat();
		dateFormatter.applyPattern(OUT_FILENAME_SUFFIX_DATETIME_FORMAT);
		Date date = new Date();
		dateFormatter.format(date);
		
		String filename = OUT_FILENAME_PREFIX + dateFormatter.format(date)
				+ OUT_FILENAME_EXTENSION;
		
		File file = new File(filename);
		
		try {
			
			if (file.exists()) {
				// quite unlikely
				throw new RuntimeException(
						String.format("%s exists on filesystem.", filename));
			}
			
			file.createNewFile();
			
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(contents);
			writer.close();
		} catch (IOException e) {
			throw new IOException("Unable to create new file.");
		}
		
		LOGGER.info(String.format("File %s written to disk.", filename));
	}
}
