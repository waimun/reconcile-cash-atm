package com.learnvest.waimun.test;

import java.math.BigDecimal;

public class BaseTransaction {

	int id;
	String name;
	BigDecimal amount;
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
}
