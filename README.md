To run the program:

java Main input.txt

(input.txt file is supplied in the src folder)

Caveats:
1. I used -1 to denote a null id because I started with int type and didn't have
time to change it to an object.
2. I did some validation on the line items but not extensively because of
time constraint. I want to assume that file feed is correct.
3. If I have 2 more hours, I would write a unit test for you.
4. I declared some constants as public because I wanted it to appear in
javadocs (if time persists) to show clients that those values can be
configurable.
5. I did not use any third party libraries because I thought you might
want to see how it works in java standard edition framework. Very unlikely
if I am coding for a production environment.
6. I did not use ThreadLocal for date formatting for simplicity and also
time constraints. Likewise, I assume date format passed is valid.
7. I made my classes and methods as self explanatory and self documenting
as possible. I would certainly write additional comments if time permits.


Let me know if you have any questions/comments.

Thank you,
Waimun Yeow