package com.learnvest.waimun.test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Transaction extends BaseTransaction
		implements CSVFileConstants, Comparable<Transaction> {
	
	private Date date;
	private boolean isCash;
	private boolean isATM;
	
	public Transaction(String[] values) throws IllegalArgumentException {
		
		if (values == null || values.length != TOTAL_LINE_COLUMNS) {
			throw new IllegalArgumentException("Values array is null or incorrect size.");
		}
		
		try {
			id = Integer.parseInt(values[0]);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid transaction id.");
		}
		
		name = values[1];
		amount = new BigDecimal(values[2]);
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat();
		dateFormatter.applyPattern(TRANSACTION_DATE_FORMAT);
		
		try {
			date = dateFormatter.parse(values[3]);
		} catch (ParseException e) {
			throw new IllegalArgumentException("Invalid transaction date.");
		}
		
		if (!isValueTrueOrFalse(values[4])) {
			throw new IllegalArgumentException("isCash must have true/false value.");
		}
		
		if (!isValueTrueOrFalse(values[5])) {
			throw new IllegalArgumentException("isATM must have true/false value.");
		}
		
		isCash = Boolean.parseBoolean(values[4]);
		isATM = Boolean.parseBoolean(values[5]);
	}
	
	private boolean isValueTrueOrFalse(String value) {
		
		return value.equalsIgnoreCase(Boolean.TRUE.toString()) ||
				value.equalsIgnoreCase(Boolean.FALSE.toString());
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public boolean isCash() {
		return isCash;
	}

	public boolean isATM() {
		return isATM;
	}

	@Override
	public int compareTo(Transaction o) {
		if (this.date.compareTo(o.date) == 0) {
			if (this.id > o.id) {
				return 1;
			} else if (this.id < o.id){
				return -1;
			} else {
				return 0;
			}
		}
		
		return this.date.compareTo(o.date);
	}
}
