package com.learnvest.waimun.test;

public interface CSVFileConstants {
	
	int TOTAL_LINE_COLUMNS = 6;
	String HEADER_ROW_PREFIX = "id";
	String TRANSACTION_DATE_FORMAT = "yyyy-MM-dd";
	String OUT_FILENAME_SUFFIX_DATETIME_FORMAT = "yyyy-MM-dd-HH-mm-ss";
	String OUT_FILENAME_PREFIX = "recon-";
	String OUT_FILENAME_EXTENSION = ".txt";
	int NULL_ID = -1;
	String COMMA = ",";
	String NEW_LINE = "\n";
}
