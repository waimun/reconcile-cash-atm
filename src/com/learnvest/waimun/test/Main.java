package com.learnvest.waimun.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;


public class Main {
	
	private static final Logger LOGGER = Logger.getLogger(Main.class.getName());
	
	public static void main(String[] args) {
		
		if (args.length != 1 || args.length > 1) {
			LOGGER.info("Usage: Main sample.csv");
			return;
		}
		
		List<String> lines;
		
		try {
			TextFileReader reader = new TextFileReader(args[0]);
			lines = reader.read(); // lines won't be null no matter what, at most empty
		} catch (IllegalArgumentException e) {
			LOGGER.severe(e.getMessage());
			return;
		} catch (FileNotFoundException e) {
			LOGGER.severe(e.getMessage());
			return;
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
			return;
		}
		
		Reconciliation recon = new Reconciliation(lines);
		
		String output = recon.displayReconciliationResults();
		System.out.println(output);
		
		// write to file
		try {
			new TextFile(output);
		} catch (IllegalArgumentException e) {
			LOGGER.info(e.getMessage());
		} catch (SecurityException e) {
			LOGGER.severe(e.getMessage());
		} catch (RuntimeException e) {
			LOGGER.severe(e.getMessage());
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
		}
	}

}
