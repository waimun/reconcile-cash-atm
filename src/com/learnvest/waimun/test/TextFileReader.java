package com.learnvest.waimun.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TextFileReader {
	
	private String filename;
	
	public TextFileReader(String filename) throws IllegalArgumentException {
		
		if (filename == null || filename.isEmpty()) {
			throw new IllegalArgumentException("Filename is null or empty.");
		}
		
		this.filename = filename;
	}
	
	public List<String> read() throws FileNotFoundException, IOException {
		
		List<String> lines = new ArrayList<String>();
		BufferedReader in = null;
		
		try {
			in = new BufferedReader(new FileReader(filename));
			while (in.ready()) {
				lines.add(in.readLine());
			}
			
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("File not found.");
		} catch (IOException e) {
			throw new IOException("An I/O exception occurred while reading file.");
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				throw new IOException("An I/O exception occurred while closing file.");
			}
		}
		
		return lines;
	}
}
