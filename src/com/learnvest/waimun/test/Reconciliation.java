package com.learnvest.waimun.test;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.logging.Logger;

public class Reconciliation implements CSVFileConstants {
	
	private static final Logger LOGGER = Logger.getLogger(Reconciliation.class.getName());
	
	private List<String> originalLineItems;
	
	private List<Transaction> transactionList;
	
	private List<Transaction> cashTransactionList;
	private List<Transaction> ATMTransactionList;
	
	private List<ReconciliationTransaction> reconciliationTransactionList;
	
	private Reconciliation() {
		transactionList = new ArrayList<Transaction>();
		cashTransactionList = new ArrayList<Transaction>();
		ATMTransactionList = new ArrayList<Transaction>();
		reconciliationTransactionList = new ArrayList<ReconciliationTransaction>();
	}
	
	public Reconciliation(List<String> lineItems) {
		this();
		
		if (lineItems != null && !lineItems.isEmpty()) {
			originalLineItems = lineItems;
			parseLines();
			sortTransactions();
			reconcile();
		}
	}
	
	public String displayReconciliationResults() {
		StringBuilder results = new StringBuilder();
		
		results.append("id,name,parent,amount");
		results.append(NEW_LINE);
		
		for (ReconciliationTransaction z : reconciliationTransactionList) {
			results.append(z.getId());
			results.append(COMMA);
			results.append(z.getName());
			results.append(COMMA);
			results.append(z.getParent());
			results.append(COMMA);
			results.append(z.getAmount());
			results.append(NEW_LINE);
		}
		
		return results.toString();
	}
	
	private void parseLines() {
		for (String lineItem : originalLineItems) {
			
			String[] values = lineItem.split(",");
			
			if (values.length != TOTAL_LINE_COLUMNS) {
				LOGGER.warning("Line does not have enough comma separated values.");
				LOGGER.warning(String.format("This line: %s will be ignored.", lineItem));
				continue;
			}
			
			if (isNotHeaderRow(lineItem)) {
				try {
					transactionList.add(new Transaction(values));
				} catch (IllegalArgumentException e) {
					LOGGER.warning(String.format("Line item is not properly formatted; %s", e.getMessage()));
					LOGGER.warning(String.format("This line: %s will be ignored.", lineItem));
				}
			}
		}
	}
	
	private void sortTransactions() {
		for (Transaction transaction : transactionList) {
			if (transaction.isCash() && !transaction.isATM()) {
				cashTransactionList.add(transaction);
			}
			
			if (transaction.isATM() && !transaction.isCash()) {
				ATMTransactionList.add(transaction);
			}
		}
		
		Collections.sort(cashTransactionList, Collections.reverseOrder());
		Collections.sort(ATMTransactionList, Collections.reverseOrder());
	}
	
	private void reconcile() {
		Deque<Transaction> stackOfcashTransactions = new ArrayDeque<Transaction>();
		stackOfcashTransactions.addAll(cashTransactionList);
		
		Deque<Transaction> stackOfATMTransactions = new ArrayDeque<Transaction>();
		stackOfATMTransactions.addAll(ATMTransactionList);
		
		while (!stackOfcashTransactions.isEmpty()) {
			
			Transaction x = stackOfcashTransactions.pop();
			Transaction y = null;
			
			if (!stackOfATMTransactions.isEmpty()) {
				y = stackOfATMTransactions.pop();
			} else {
				ReconciliationTransaction z = new ReconciliationTransaction(
						x.getId(), x.getName(), NULL_ID, x.getAmount());
				
				reconciliationTransactionList.add(z);
				continue;
			}
			
			if (y.getDate().compareTo(x.getDate()) <= 0) {
				
				// rule 1
				if (x.getAmount().compareTo(y.getAmount()) < 0) {
					ReconciliationTransaction z = new ReconciliationTransaction(
							x.getId(), x.getName(), y.getId(), x.getAmount());
					
					reconciliationTransactionList.add(z);
					y.setAmount(y.getAmount().subtract(x.getAmount()));
					stackOfATMTransactions.push(y);
				}
				
				// rule 2
				if (x.getAmount().compareTo(y.getAmount()) == 0) {
					ReconciliationTransaction z = new ReconciliationTransaction(
							x.getId(), x.getName(), y.getId(), x.getAmount());
					
					reconciliationTransactionList.add(z);
				}
				
				// rule 3
				if (x.getAmount().compareTo(y.getAmount()) > 0) {
					ReconciliationTransaction z = new ReconciliationTransaction(
							x.getId(), x.getName(), y.getId(), y.getAmount());
					
					reconciliationTransactionList.add(z);
					x.setAmount(x.getAmount().subtract(y.getAmount()));
					stackOfcashTransactions.push(x);
				}
			}
		}
	}
	
	private boolean isNotHeaderRow(String lineItem) {
		return !lineItem.startsWith(HEADER_ROW_PREFIX);
	}
}
