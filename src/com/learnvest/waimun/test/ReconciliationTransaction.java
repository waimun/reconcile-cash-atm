package com.learnvest.waimun.test;

import java.math.BigDecimal;

public class ReconciliationTransaction extends BaseTransaction {
	
	private int parent;
	
	public ReconciliationTransaction(int id, String name, int parent,
			BigDecimal amount) {
		
		this.id = id;
		this.name = name;
		this.parent = parent;
		this.amount = amount;
	}
	
	public int getParent() {
		return parent;
	}	
}
